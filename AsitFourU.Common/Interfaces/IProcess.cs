﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsitFourU.Common
{
    public interface IProcess
    {
        /// <summary>
        /// Executes the process.
        /// </summary>
        void ExecuteProcess();

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <returns></returns>
        object GetParameters();

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <returns></returns>
        string GetName();

        /// <summary>
        /// Determines whether this instance is repeatable.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is repeatable; otherwise, <c>false</c>.
        /// </returns>
        bool IsRepeatable();

        //Schedule Object Required Here
        /// <summary>
        /// Gets the repetition interval time.
        /// </summary>
        /// <returns></returns>
        int GetRepetitionIntervalTime();

    }
}
