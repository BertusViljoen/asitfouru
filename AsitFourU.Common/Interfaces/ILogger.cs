﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsitFourU.Common
{
    public interface ILogger
    {
        /// <summary>
        /// Debugs the specified log message.
        /// </summary>
        /// <param name="logMessage">The log message.</param>
        void Debug(string logMessage);

        /// <summary>
        /// Errors the specified log message.
        /// </summary>
        /// <param name="logMessage">The log message.</param>
        void Error(string logMessage);
    }
}
