﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsitFourU.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class ProcessManager
    {

        //Logger Required Here
        ILogger logger = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessManager"/> class.
        /// </summary>
        public ProcessManager(ILogger logger = null)
        {
            if (logger == null) logger = new SimpleLogger();
        }

        /// <summary>
        /// Executes all process.
        /// </summary>
        public void ExecuteAllProcess()
        {
            logger.Debug("Begin Execute All Processes");

            try
            {
                //Get List of all IProcess implementations
                IEnumerable<Type> Processes = GetAllTypesImplementingInterface(typeof(IProcess));

                //Execute each Process
                if (Processes != null && Processes.Count() > 0)
                {
                    IProcess Process = null;
                    Thread thread = null;
                    foreach (var process in Processes) 
                    {
                        if (IsRealClass(process))
                        {
                            try
                            {
                                //Initiate Process
                                Process = (IProcess)Activator.CreateInstance<IProcess>();
                                logger.Debug($"The Process {Process.GetName()} has been instantiated successfully");
                                thread = new Thread(new ThreadStart(Process.ExecuteProcess));
                                thread.Start();
                                logger.Debug($"The Process {Process.GetName()} has started");
                            }
                            catch (Exception ex)
                            {
                                logger.Error(ex.ToString());
                            }
                        }
                    }
                }
                else
                {
                    logger.Debug("No Processes Loaded");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        /// <summary>
        /// Gets all types implementing interface.
        /// </summary>
        /// <param name="desireType">Type of the desire.</param>
        /// <returns></returns>
        private IEnumerable<Type> GetAllTypesImplementingInterface(Type desireType)
        {
            return AppDomain
                .CurrentDomain
                .GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .Where(type => typeof(IProcess).IsAssignableFrom(type));
        }

        /// <summary>
        /// Determines whether [is real class] [the specified test type].
        /// </summary>
        /// <param name="testType">Type of the test.</param>
        /// <returns>
        ///   <c>true</c> if [is real class] [the specified test type]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsRealClass(Type testType)
        {
            return testType.IsAbstract == false
                && testType.IsGenericTypeDefinition == false
                && testType.IsInterface == false;
        }
    }
}
