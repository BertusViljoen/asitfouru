﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsitFourU.Common
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ProcessBase : IProcess
    {
        /// <summary>
        /// Executes the process.
        /// </summary>
        public void ExecuteProcess()
        {

        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual string GetName()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual object GetParameters()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the repetition interval time.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public int GetRepetitionIntervalTime()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Determines whether this instance is repeatable.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is repeatable; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool IsRepeatable()
        {
            throw new NotImplementedException();
        }
    }
}
