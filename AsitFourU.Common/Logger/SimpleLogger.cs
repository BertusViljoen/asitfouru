﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsitFourU.Common
{
    public class SimpleLogger : ILogger
    {
        public SimpleLogger()
        {

        }

        public void Debug(string logMessage)
        {
            Console.WriteLine($"Debug: {logMessage}");
        }

        public void Error(string logMessage)
        {
            Console.WriteLine($"Error: {logMessage}");
        }
    }
}
