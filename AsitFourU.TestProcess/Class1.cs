﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsitFourU.Common;
using System.ComponentModel.Composition;

namespace AsitFourU.TestProcess
{
    [Export(typeof(IProcess))]
    public class TestProcess : IProcess
    {
        public TestProcess()
        {

        }

        public void ExecuteProcess()
        {
            Console.WriteLine("Executing Process");
        }

        public string GetName()
        {
            return this.ToString();
        }

        public object GetParameters()
        {
            return new object();
        }

        public int GetRepetitionIntervalTime()
        {
            return 0;
        }

        public bool IsRepeatable()
        {
            return false;
        }
    }
}
